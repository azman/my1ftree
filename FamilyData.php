<?php
//------------------------------------------------------------------------------
// base class for family data
require_once dirname(__FILE__).'/FamilyCore.php';
//------------------------------------------------------------------------------
class FamilyData extends FamilyObject {
	public $logins;
	public $people;
	public $unions;
	public $ismain;
	public $dofile;
	function __construct() {
		$this->reset();
	}
	function reset() {
		$this->logins = [];
		$this->people = [];
		$this->unions = [];
		$this->ismain = true; // assume main data!
		$this->dofile = null;
	}
	function countP() {
		return count($this->people);
	}
	function countU() {
		return count($this->unions);
	}
	function Person($ppid,$copy=false) {
		if (!is_int($ppid))
			$this->throw_debug("NOT PersonID?!");
		$that = null;
		foreach ($this->people as &$temp) {
			if ($temp->Id()==$ppid) {
				if ($copy===true) {
					$that = new Person();
					$that->copy($temp);
				}
				else $that = &$temp;
				break;
			}
		}
		return $that;
	}
	function Union($uuid,$copy=false) {
		if (!is_int($uuid))
			$this->throw_debug("NOT UnionID?!");
		$that = null;
		foreach ($this->unions as &$temp) {
			if ($temp->Id()==$uuid) {
				if ($copy===true) {
					$that = new Union();
					$that->copy($temp);
				}
				else $that = &$temp;
				break;
			}
		}
		return $that;
	}
	function Login($name) {
		foreach ($this->logins as $temp) {
			if ($temp->Name()==$name) {
				return $temp;
			}
		}
		return null;
	}
	function assignData($data) { // json decoded data
		// main requirement
		if (!isset($data['countP'])||!isset($data['countU']))
			return false;
		// clear existing data
		$this->reset();
		$datP = $data['countP']; $datU = $data['countU'];
		unset($data['countP']); unset($data['countU']);
		$chkP = 0; $chkU = 0;
		foreach ($data as $key => $val) {
			// check person data
			if (substr($key,0,6)=="person") {
				$person = new Person($val);
				if (!$person->is_valid())
					$this->throw_debug("Invalid Person data! (".
						json_encode($val).")");
				$test = $this->Person($person->Id());
				if ($test!==null)
					$this->throw_debug("Person exists! (".
						json_encode($val).")");
				if (isset($person->base['gid']))
					$this->ismain = false;
				array_push($this->people,$person);
				$chkP++;
			}
			// check union data
			else if (substr($key,0,5)=="union") {
				$union = new Union($val);
				if (!$union->is_valid())
					$this->throw_debug("Invalid Union data! (".
						json_encode($val).")");
				$test = $this->Union($union->Id());
				if ($test!==null)
					$this->throw_debug("Union exists! (".
						json_encode($val).")");
				if (isset($union->base['gid']))
					$this->ismain = false;
				array_push($this->unions,$union);
				$chkU++;
			}
			// check login data - support old format
			else if (substr($key,0,6)=="logins") {
				foreach ($val as $temp) {
					$login = new Login($temp);
					if (!$login->is_valid())
						$this->throw_debug("Invalid Login data! (".
							json_encode($temp).")");
					$test = $this->Login($login->Name());
					if ($test!==null)
						$this->throw_debug("Login exists! (".
							json_encode($temp).")");
					array_push($this->logins,$login);
				}
			}
			// check login data
			else if (substr($key,0,5)=="login") {
				$login = new Login($val);
				if (!$login->is_valid())
					$this->throw_debug("Invalid Login data! (".
						json_encode($val).")");
				$test = $this->Login($login->Name());
				if ($test!==null)
					$this->throw_debug("Login exists! (".
						json_encode($val).")");
				// unique login info
				array_push($this->logins,$login);
			}
			else $this->throw_debug("Unknown key! ($key)");
		}
		if (($chkP!==$datP)||($chkU!==$datU))
			return false;
		return true;
	}
	function updateData($that) {
		if (!is_subclass_of($that,'FamilyData'))
			$this->throw_debug("NOT a FamilyData!");
		foreach ($that->people as $pchk) {
			$test = $this->Person($pchk->Id());
			if ($test===null)  // new Person!
				array_push($this->people,$pchk);
			else {
				// in case this is from sub-data (parent was left out)
				if ($pchk->PCount()<$test->PCount()) {
					foreach ($test->link as $key => $val) {
						if (substr($key,0,7)=="parents")
							$pchk->AddP(intval($val));
					}
				}
				$test->copy($pchk); // update
			}
		}
		foreach ($that->unions as $uchk) {
			$test = $this->Union($uchk->Id());
			if ($test===null)  // new Union!
				array_push($this->unions,$uchk);
			else $test->copy($uchk); // update
		}
	}
	function accessFile($filename) {
		$data = file_get_contents($filename);
		$json = json_decode($data,true);
		$this->assignData($json);
		$this->valid8Data();
		$this->dofile = $filename;
		// return text data - in case needed
		return $data;
	}
	function updateFile($data) { // text format
		if ($this->dofile===null||!file_exists($this->dofile)||
				!is_writable($this->dofile)) {
			$this->throw_debug("Cannot write data!");
		}
		$json = json_decode($data,true);
		$that = new FamilyData();
		$that->assignData($json);
		$this->updateData($that);
		$this->valid8Data();
		$text = $this->toJSON();
		file_put_contents($this->dofile,$text,LOCK_EX);
	}
	function valid8Data() {
		$inum = []; $istr = [];
		foreach ($this->people as $that) {
			// check duplicate ids
			$pnum = $that->StrIdNum();
			if (isset($inum[$pnum]))
				$this->throw_debug("Duplicate ($pnum)!");
			$inum[$pnum] = true;
			$pstr = $that->StrId();
			if (isset($istr[$pstr]))
				$this->throw_debug("Duplicate ($pstr)!");
			$istr[$pstr] = true;
			foreach ($that->link as $key1 => $val1) {
				if (substr($key1,0,6)=="unions"||
						substr($key1,0,7)=="parents") {
					$look = intval($val1);
					if ($this->Union($look)===null)
						$this->throw_debug("Missing union (Id:$look)!");
				}
			}
		}
		$inum = []; $istr = [];
		foreach ($this->unions as $that) {
			// check duplicate ids
			$pnum = $that->StrIdNum();
			if (isset($inum[$pnum]))
				$this->throw_debug("Duplicate ($pnum)!");
			$inum[$pnum] = true;
			$pstr = $that->StrId();
			if (isset($istr[$pstr]))
				$this->throw_debug("Duplicate ($pstr)!");
			$istr[$pstr] = true;
			$look = intval($that->base['fatherid']);
			if ($this->Person($look)===null)
				$this->throw_debug("Missing personF (Id:$look)!");
			$look = intval($that->base['motherid']);
			if ($this->Person($look)===null)
				$this->throw_debug("Missing personM (Id:$look)!");
			foreach ($that->link as $key1 => $val1) {
				if (substr($key1,0,5)=="child") {
					$look = intval($val1);
					if ($this->Person($look)===null)
						$this->throw_debug("Missing personC (Id:$look)!");
				}
			}
		}
	}
	function writeJSON($secret=false,$nologs=false) {
		$text = "{\n";
		if ($nologs===false) {
			$step = 1;
			foreach ($this->logins as $that) {
				$temp = "\"login".$step."\":".$that->jsonstr().",\n";
				$text = $text.$temp;
				$step++;
			}
		}
		$text = $text."\"countP\":".count($this->people);
		$step = 1;
		foreach ($this->people as $that) {
			if ($secret) $that->dosecret();
			$temp = ",\n\"person".$step."\":".$that->jsonstr();
			$text = $text.$temp;
			$step++;
		}
		$text = $text.",\n\"countU\":".count($this->unions);
		$step = 1;
		foreach ($this->unions as $that) {
			if ($secret) $that->dosecret();
			$temp = ",\n\"union".$step."\":".$that->jsonstr();
			$text = $text.$temp;
			$step++;
		}
		$text = $text."\n}\n";
		return $text;
	}
}
//------------------------------------------------------------------------------
?>

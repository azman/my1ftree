<?php
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		// returns html only if NOT on console?
		header('Content-Type: text/html; charset=utf-8');
		echo "<h1><p>Invalid access!</p></h1>".PHP_EOL;
		exit();
	}
	if ($argc<2)
		throw new Exception("Not enough argument!");
	$from = $argv[1];
	$user = 1; // pick first person
	$type = 4; // viewer mode
	$syyy = false;
	$outs = null;
	$info = true;
	$val8 = true;
	for ($loop=2;$loop<$argc;$loop++) {
		if ($argv[$loop]==='--syyy'||$argv[$loop]==='--secret')
			$syyy = true;
		else if ($argv[$loop]==='--data')
			$info = false;
		else if ($argv[$loop]==='--debug')
			$val8 = false;
		else if ($argv[$loop]==='-o'||$argv[$loop]==='--output') {
			if (++$loop>=$argc)
				throw new Exception("** No value for ".$argv[$loop-1]."!");
			$outs = $argv[$loop];
		}
		else if ($argv[$loop]==='--user') {
			if (++$loop>=$argc)
				throw new Exception("** No value for ".$argv[$loop-1]."!");
			$user = intval($argv[$loop]);
		}
		else if ($argv[$loop]==='--type') {
			if (++$loop>=$argc)
				throw new Exception("** No value for ".$argv[$loop-1]."!");
			$type = intval($argv[$loop]);
		}
		else throw new Exception("** Unknown option (".$argv[$loop].")!");
	}
	if (!file_exists($from))
		throw new Exception("** File '$from' not found!");
	require_once dirname(__FILE__).'/FamilyX.php';
	$temp = new FamilyX();
	$temp->accessFile($from);
	$buff = $temp->filterFamily($user,$type,$val8);
	if ($info!==false) {
		echo "-- Read: ".$from.PHP_EOL;
		echo "CountP: ".count($temp->people).PHP_EOL;
		echo "CountU: ".count($temp->unions).PHP_EOL;
		echo "-- Pass: ".PHP_EOL;
		echo "UserID: ".$user.PHP_EOL;
		echo "FilterType: ".$type.PHP_EOL;
		echo "CountP: ".count($buff->people).PHP_EOL;
		echo "CountU: ".count($buff->unions).PHP_EOL;
		if ($outs===null) echo "Data: ".PHP_EOL;
		else echo "-- File: ".$outs.PHP_EOL;
	}
	$text = $buff->writeJSON($syyy);
	if ($outs===null) echo $text;
	else file_put_contents($outs,$text);
	if (!$val8) $buff->valid8Data();
} catch( Exception $error ) {
	echo "Execution error! [".$error->getMessage()."]".PHP_EOL;
}
exit();
?>

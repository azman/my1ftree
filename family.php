<?php
function define_if_not_exists($stuff,$value) {
	defined($stuff) or define($stuff,$value);
}
define('DOCONFIG',(__FILE__).'.conf');
if (file_exists(DOCONFIG)) include_once DOCONFIG;
define_if_not_exists('ERRORAPI','Invalid API!');
define_if_not_exists('DEBUGAPI',false);
define('HTMLVIEW','family.html');
define('CHKLOGIN','family_login.html');
define('HTMLDBEG','<!--BEGIN-MY1DATA-AREA-->');
define('HTMLDEND','<!--END-MY1DATA-AREA-->');
function error_exit($message) {
	if (empty($message)) $message = 'Unknown exception!';
	if (DEBUGAPI===false) $message = ERRORAPI;
	throw new Exception($message);
}
define('MY1_USER','my1family_username');
define('MY1_PASS','my1family_userpass');
function my1session_init() {
	if (session_status()===PHP_SESSION_NONE)
		session_start();
}
function my1session_done() {
	my1session_init();
	if (isset($_SESSION[MY1_USER])) {
		unset($_SESSION[MY1_USER]);
	}
	if (isset($_SESSION[MY1_PASS])) {
		unset($_SESSION[MY1_PASS]);
	}
}
try {
	$method = 'GET';
	if (isset($_SERVER['REQUEST_METHOD']))
		$method = $_SERVER['REQUEST_METHOD'];
	$target = $_SERVER['REQUEST_URI'];
	$chksrv = dirname($target);
	$topchk = dirname($_SERVER['SCRIPT_FILENAME']);
	$topchk = explode('/', $topchk);
	$topchk = array_pop($topchk);
	$params = explode('/', $target);
	do {
		$chkchk = array_shift($params); // find api top folder
		if ($chkchk===false)
			error_exit("Cannot find API top folder!");
	} while($chkchk!=$topchk);
	// option to request json format
	$dojson = null;
	// get family name
	$family = array_shift($params);
	// get option
	$option = array_shift($params);
	if (!empty($option)) {
		$option = strtolower($option);
		$chksrv = dirname($chksrv);
		if ($option==="logout") {
			my1session_done();
			header('Location: '.$chksrv);
			exit();
		}
		else if ($option==="json") $dojson = $option;
		else error_exit("Invalid option '".$option."'!");
	}
	if ($chksrv==="/") $chksrv = "";
	// params should be empty by now
	if (!empty($params)) error_exit("Invalid params!");
	// check request
	switch ($method) {
	case 'GET':
		// get viewer html
		if (!file_exists(HTMLVIEW))
			error_exit("Viewer page NOT found!");
		// send viewer template for empty requests
		if (empty($family)) {
			readfile(dirname(__FILE__)."/".HTMLVIEW);
			exit();
		}
		// look for family
		$doname = $family.".json";
		if (!file_exists($doname))
			error_exit("Invalid family '".$family."'!");
		// use family class
		require_once dirname(__FILE__).'/FamilyX.php';
		$domain = new FamilyX();
		$dodata = $domain->accessFile($doname);
		// check if login-'protected'
		if ($domain->doLogin()) {
			my1session_init();
			if (!isset($_SESSION[MY1_USER])||!isset($_SESSION[MY1_PASS])||
					!isset($_SESSION['requestz'])) {
				if (!file_exists(CHKLOGIN))
					error_exit("Login page NOT found!");
				$dologs = file_get_contents(CHKLOGIN);
				$dologs = preg_replace("/^(var family =).*$/m",
					"$1 \"".$family."\";",$dologs);
				if (!empty($dojson)) {
					$dologs = preg_replace("/^(var format =).*$/m",
						"$1 \"".$dojson."\";",$dologs);
				}
				$dologs = preg_replace("/^(var doreqs =).*$/m",
					"$1 \"".$chksrv."\";",$dologs);
				$dologs = preg_replace("/family.php/m",$chksrv."/",$dologs);
				echo $dologs;
				exit();
			}
			// check request
			$chkreq = $chksrv."/".$family;
			if (!empty($dojson))
				$chkreq = $chkreq."/".$dojson;
			if ($_SESSION['requestz']!==$chkreq)
				error_exit("Invalid session '".$_SESSION['requestz']."'!");
			// check login
			$user = $domain->validateUser($_SESSION[MY1_USER],
				$_SESSION[MY1_PASS],
				isset($_SESSION['passnew1'])?$_SESSION['passnew1']:null,
				isset($_SESSION['passnew2'])?$_SESSION['passnew2']:null);
			// clear any password change request
			unset($_SESSION['passnew1']);
			unset($_SESSION['passnew2']);
			if ($user===null)
				error_exit("Invalid login '".$_SESSION[MY1_USER]."'!");
			if ($user->Type()>1&&$domain->ismain) {
				$filter = $domain->filterFamily($user->Id(),$user->Type());
				$dodata = $filter->writeJSON(false,true);
				if (!$filter->ismain) {
					$find = $domain->gPerson($user->base['id']);
					if ($find===null) $user->base['id'] = 1;
					else $user->base['id'] = $find->Id();
				}
				$filter = null;
			}
			else $dodata = $domain->writeJSON(false,true);
		}
		else my1session_done();
		// check for raw json request
		if (!empty($dojson)) {
			if ($user->Type()>1) error_exit("Not privileged!");
			header('Content-Type: text/json; charset=utf-8');
			echo $dodata;
			exit();
		}
		// load html page
		$doview = file_get_contents(HTMLVIEW);
		// edit html if applicable
		if (isset($user)&&$user!==null) {
			// not all can update server - need FamilyData for this!
			if ($user->Type()>0&&$user->Type()<4) {
				// update server access flag
				$doview = preg_replace("/^(var isServer =).*$/m",
					"$1 true;",$doview);
				$doview = preg_replace("/^(var baseName =).*$/m",
					"$1 \"".$family."\";",$doview);
			}
			// update login user data selection
			$doview = preg_replace("/^(var thisRoot =).*$/m",
				"$1 ".$user->Id().";",$doview);
			// just insert into html!
			$idx0 = strpos($doview,HTMLDBEG)+strlen(HTMLDBEG);
			$idx1 = strpos($doview,HTMLDEND);
			echo substr($doview,0,$idx0)."\n";
			echo "<div id=\"my1data\">\n<!--\n";
			echo $dodata;
			echo "-->\n</div>\n";
			echo substr($doview,$idx1);
		}
		break;
	case 'POST':
		// is it a login post?
		if (array_key_exists('username',$_POST)&&
				array_key_exists('password',$_POST)&&
				array_key_exists('requestz',$_POST)) {
			// login info - not authenticated here, just start a session
			my1session_init();
			$_SESSION[MY1_USER] = $_POST['username'];
			$_SESSION[MY1_PASS] = $_POST['password'];
			$_SESSION['requestz'] = $_POST['requestz'];
			unset($_SESSION['passnew1']);
			unset($_SESSION['passnew2']);
			if (!empty($_POST['passnew1'])&&!empty($_POST['passnew2'])&&
					$_POST['passnew1']===$_POST['passnew2']) {
				$_SESSION['passnew1'] = $_POST['passnew1'];
				$_SESSION['passnew2'] = $_POST['passnew2'];
			}
			header('Location: '.$_POST['requestz']);
			exit();
		}
		// or, is it a data post?
		if ($family==="") error_exit("No family specified!");
		$doname = $family.".json";
		if (!file_exists($doname))
			error_exit("Invalid family '".$family."'!");
		else if (!is_writeable($doname))
			error_exit("Cannot update '".$family."'!");
		// get existing data
		require_once dirname(__FILE__).'/FamilyX.php';
		$domain = new FamilyX();
		$domain->accessFile($doname);
		// check if login-'protected'
		if ($domain->doLogin()) {
			my1session_init();
			if (!isset($_SESSION[MY1_USER])||!isset($_SESSION[MY1_PASS]))
				error_exit("Invalid session to post data!");
			// check login
			$user = $domain->validateUser(
				$_SESSION[MY1_USER],$_SESSION[MY1_PASS]);
			if ($user===null)
				error_exit("Invalid permission to update '".$family."'!");
		}
		// cannot update if NOT login protected?
		else error_exit("Invalid post?!");
		// get updated content -raw http
		$dodata = file_get_contents("php://input");
		$dotest = json_decode($dodata,true);
		$donext = new FamilyX();
		$donext->assignData($dotest);
		$donext->domainData($domain);
		$domain->updateData($donext);
		$domain->valid8Data();
		$dodata = $domain->writeJSON();
		file_put_contents($doname,$dodata,LOCK_EX);
		break;
	default:
		error_exit('Invalid Request!');
	}
} catch (Exception $errmsg) {
	// uncomment the next 2 lines to clean up session on error?
	my1session_done();
	echo "<!DOCTYPE html>\n";
	header("HTTP/1.0 404 Not Found");
	header('Content-Type: text/html; charset=utf-8');
	echo "<h1>ERROR: ".$errmsg->getMessage()."</h1>\n";
}
exit();
?>

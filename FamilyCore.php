<?php
//------------------------------------------------------------------------------
// core component classes for FamilyData
//------------------------------------------------------------------------------
class FamilyObject {
	protected function throw_debug($error) {
		throw new Exception('['.get_class($this).'] => {'.$error.'}');
	}
}
//------------------------------------------------------------------------------
class FamilyItem extends FamilyObject {
	public $base;
	public $xtra;
	public $link;
	public $more;
	protected $_valid;
	function __construct($read=null) {
		$this->validate($read);
	}
	function dosecret() {
		$this->xtra = [];
		$this->more = [];
	}
	function is_valid() {
		return $this->_valid;
	}
	protected function reset() {
		$this->base = [];
		$this->xtra = [];
		$this->link = [];
		$this->more = [];
		$this->_valid = false;
	}
	protected function check($key,$val) {
		$this->throw_debug("Override this!");
	}
	protected function valid() {
		return true;
	}
	function validate($read=null) {
		if ($read==null) return;
		$this->reset();
		foreach ($read as $key => $val) {
			$this->check($key,$val);
		}
		$this->_valid = $this->valid();
		return $this->_valid;
	}
	function copy($from) {
		if (!is_subclass_of($from,'FamilyItem'))
			$this->throw_debug("NOT a FamilyItem!");
		if (!$from->is_valid())
			$this->throw_debug("Invalid FamilyItem!");
		$this->base = $from->base;
		$this->xtra = $from->xtra;
		$this->link = $from->link;
		$this->more = $from->more;
		$this->_valid = true;
	}
	function itemize() {
		$item = [];
		foreach ($this->base as $key => $val)
			$item[$key] = $val;
		foreach ($this->xtra as $key => $val)
			$item[$key] = $val;
		foreach ($this->link as $key => $val)
			$item[$key] = $val;
		foreach ($this->more as $key => $val)
			$item[$key] = $val;
		return $item;
	}
	function jsonstr() {
		return json_encode($this->itemize());
	}
}
//------------------------------------------------------------------------------
class Person extends FamilyItem {
	function __construct($read=null) {
		parent::__construct($read);
	}
	protected function check($key,$val) {
		if ($key=='id'||$key=='gender'||$key=='name'||$key=='gid')
			$this->base[$key] = $val;
		else if ($key=='fullname'||$key=='bdate'||$key=='ddate')
			$this->xtra[$key] = $val;
		else if ($key=='pcount'||$key=='ucount'||
				substr($key,0,7)=="parents"||substr($key,0,6)=="unions")
			$this->link[$key] = $val;
		else if ($key=='info')
			$this->more[$key] = $val;
		else
			$this->throw_debug("Unknown Person property! ($key)");
	}
	protected function valid() {
		$stat = true;
		if (!isset($this->base['id'])||!is_int($this->base['id'])||
				!isset($this->base['gender'])||!is_int($this->base['gender'])||
				!isset($this->base['name'])||
				!isset($this->link['pcount'])||
				!is_int($this->link['pcount'])||
				!isset($this->link['ucount'])||
				!is_int($this->link['ucount'])) {
			$stat = false;
		}
		else {
			$chkP = 0; $chkU = 0;
			foreach ($this->link as $key => $val) {
				if (substr($key,0,7)=="parents") $chkP++;
				else if (substr($key,0,6)=="unions") $chkU++;
			}
			if ($chkP!=$this->link['pcount']||$chkU!=$this->link['ucount'])
				$stat = false;
		}
		if (isset($this->base['gid'])&&!is_int($this->base['gid']))
			$stat = false;
		return $stat;
	}
	function Id() {
		return $this->base['id'];
	}
	function gId() {
		return isset($this->base['gid'])?$this->base['gid']:0;
	}
	function StrId() {
		$istr = "person_".$this->base['gender']."_".$this->base['name'];
		if ($this->link['pcount']>0)
			$istr = $istr."_up".$this->link['parents1'];
		if ($this->link['ucount']>0)
			$istr = $istr."_un".$this->link['unions1'];
		$istr = $istr."_".$this->base['id'];
		if (isset($this->base['gid']))
			$istr = $istr."_g".$this->base['gid'];
		return $istr;
	}
	function StrIdNum() {
		$istr = "p".$this->base['id'];
		if (isset($this->base['gid']))
			$istr = $istr."_g".$this->base['gid'];
		return $istr;
	}
	function Id2gId($idnew) {
		$this->base['gid'] = $this->base['id'];
		$this->base['id'] = intval($idnew);
	}
	function PCount() {
		return $this->link['pcount']; // should always be available
	}
	function UCount() {
		return $this->link['ucount'];
	}
	function niceLinks() {
		// rearrange links
		$list = [];
		// parents first
		$list['pcount'] = $this->link['pcount'];
		foreach ($this->link as $key => $val) {
			if (substr($key,0,7)=="parents")
				$list[$key] = $val;
		}
		$list['ucount'] = $this->link['ucount'];
		foreach ($this->link as $key => $val) {
			if (substr($key,0,6)=="unions")
				$list[$key] = $val;
		}
		$this->link = $list;
	}
	function AddP($id) {
		// assume valid id
		$step = $this->link['pcount']+1;
		$this->link['parents'.$step] = $id;
		$this->link['pcount']++;
		$this->niceLinks();
	}
}
//------------------------------------------------------------------------------
class Union extends FamilyItem {
	function __construct($read=null) {
		parent::__construct($read);
	}
	protected function check($key,$val) {
		if ($key=='id'||$key=='fatherid'||$key=='motherid'||$key=='gid')
			$this->base[$key] = $val;
		else if ($key=='mdate'||$key=='ddate')
			$this->xtra[$key] = $val;
		else if ($key=='ccount'||substr($key,0,5)=="child")
			$this->link[$key] = $val;
		else if ($key=='info')
			$this->more[$key] = $val;
		else
			$this->throw_debug("Unknown Union property! ($key)");
	}
	protected function valid() {
		$stat = true;
		if (!isset($this->base['id'])||!is_int($this->base['id'])||
				!isset($this->base['fatherid'])||
				!is_int($this->base['fatherid'])||
				!isset($this->base['motherid'])||
				!is_int($this->base['motherid'])||
				!isset($this->link['ccount'])||
				!is_int($this->link['ccount'])) {
			$stat = false;
		}
		else {
			$chkC = 0;
			foreach ($this->link as $key => $val) {
				if (substr($key,0,5)=="child") $chkC++;
			}
			if ($chkC!=$this->link['ccount']) $stat = false;
		}
		if (isset($this->base['gid'])&&!is_int($this->base['gid']))
			$stat = false;
		return $stat;
	}
	function Id() {
		return $this->base['id'];
	}
	function gId() {
		return isset($this->base['gid'])?$this->base['gid']:0;
	}
	function IdF() {
		return $this->base['fatherid'];
	}
	function IdM() {
		return $this->base['motherid'];
	}
	function StrId() {
		$istr = "union";
		$istr = $istr."_f".$this->base['fatherid'];
		$istr = $istr."_m".$this->base['motherid'];
		$istr = $istr."_".$this->base['id'];
		if (isset($this->base['gid']))
			$istr = $istr."_g".$this->base['gid'];
		return $istr;
	}
	function StrIdNum() {
		$istr = "u".$this->base['id'];
		if (isset($this->base['gid']))
			$istr = $istr."_g".$this->base['gid'];
		return $istr;
	}
	function Id2gId($idnew) {
		$this->base['gid'] = $this->base['id'];
		$this->base['id'] = intval($idnew);
	}
}
//------------------------------------------------------------------------------
class Login extends FamilyItem {
	function __construct($read=null) {
		parent::__construct($read);
	}
	protected function check($key,$val) {
		if ($key=='id'||$key=='type'||$key=='user'||$key=='pass')
			$this->base[$key] = $val;
		else
			$this->throw_debug("Unknown Login property! ($key)");
	}
	protected function valid() {
		$stat = true;
		if (!isset($this->base['id'])||!is_int($this->base['id'])||
				!isset($this->base['type'])||!is_int($this->base['type'])||
				!isset($this->base['user'])||!isset($this->base['pass'])) {
			$stat = false;
		}
		return $stat;
	}
	function Id() {
		return $this->base['id'];
	}
	function Type() {
		return $this->base['type'];
	}
	function Name() {
		return $this->base['user'];
	}
	function Pass() {
		return $this->base['pass'];
	}
}
//------------------------------------------------------------------------------
?>

<?php
//------------------------------------------------------------------------------
// class for family tree
require_once dirname(__FILE__).'/FamilyData.php';
//------------------------------------------------------------------------------
class FamilyX extends FamilyData {
	function __construct() {
		parent::__construct();
	}
	function addPerson($data) {
		if (!$data instanceof Person)
			$this->throw_debug("NOT Person data!");
		array_push($this->people,$data);
	}
	function addUnion($data) {
		if (!$data instanceof Union)
			$this->throw_debug("NOT Union data!");
		array_push($this->unions,$data);
	}
	function gPerson($ppid,$copy=false) {
		$that = null;
		foreach ($this->people as &$temp) {
			if ($temp->gId()==$ppid) {
				if ($copy===true) {
					$that = new Person();
					$that->copy($temp);
				}
				else $that = &$temp;
				break;
			}
		}
		return $that;
	}
	function gUnion($uuid,$copy=false) {
		$that = null;
		foreach ($this->unions as &$temp) {
			if ($temp->gId()==$uuid) {
				if ($copy===true) {
					$that = new Union();
					$that->copy($temp);
				}
				else $that = &$temp;
				break;
			}
		}
		return $that;
	}
	function doLogin() {
		return count($this->logins)>0?true:false;
	}
	function validateUser($name,$pass,$new1=null,$new2=null) {
		$user = null;
		foreach ($this->logins as &$info) {
			if ($info->Name()===$name) {
				if ($info->Pass()===$pass) {
					$user = new Login();
					$user->copy($info);
					unset($user->base['pass']);
					if ($new1!==null&&$new2!==null&&$new1===$new2) {
						$info->base['pass'] = $new1;
						// replace in file
						$temp = file_get_contents($this->dofile);
						$that = "\"login".$info->Id()."\":";
						$next = $that.$info->jsonstr()."\n";
						$find = "\"login.*id=\"".$info->Id()."\".*";
						if (preg_match($that,$temp)) {
							$temp = preg_replace("/^".$find."$(?:\r\n|\n)?/m",
								$next,$temp);
						}
						else {
							$pick = "^\"countP\".*$(?:\r\n|\n)?/m";
							$temp = preg_replace($pick,$next."\n${1}",$temp);
						}
						$test = file_put_contents($this->dofile,$temp);
						if ($test===FALSE)
							$this->throw_debug("Cannot write file!");
					}
				}
				break;
			}
		}
		return $user;
	}
	function checkAncestor($pcur,$pchk) {
		if ($this->Person($pcur->Id())===null||
				$this->Person($pchk->Id())===null) {
			$this->throw_debug("Not family member(s)!");
		}
		$checked = false;
		foreach ($pcur->link as $key1 => $val1) {
			if (substr($key1,0,7)=="parents") {
				$val1 = intval($val1);
				$uchk = $this->Union($val1);
				$par1 = $this->Person($uchk->idF());
				if ($par1->Id()===$pchk->Id()||
						$this->checkAncestor($par1,$pchk)) {
					$checked = true;
					break;
				}
				$par2 = $this->Person($uchk->IdM());
				if ($par2->Id()===$pchk->Id()||
						$this->checkAncestor($par2,$pchk)) {
					$checked = true;
					break;
				}
			}
		}
		return $checked;
	}
	function checkDescendent($pcur,$pchk) {
		if ($this->Person($pcur->Id())===null||
				$this->Person($pchk->Id())===null) {
			$this->throw_debug("Not family member(s)!");
		}
		$checked = false;
		foreach ($pcur->link as $key1 => $val1) {
			if (substr($key1,0,6)=="unions") {
				$val1 = intval($val1);
				$uchk = $this->Union($val1);
				foreach ($uchk as $key2 => $val2) {
					if (substr($key1,0,5)=="child") {
						$val2 = intval($val2);
						$chi1 = $this->Person($val2);
						if ($chi1->Id()===$pchk->Id()||
								$this->checkDescendent($chi1,$pchk)) {
							$checked = true;
							break;
						}
					}
				}
			}
		}
		return $checked;
	}
	function filterAncestor($pchk,$type,$root,$clvl) {
		// only filter/type levels 1,2,3 can view ancestors
		// - level 3 gets parents of initial person/spouse & descendents?
		// - level 2 gets initial person's ancestors
		if (($type==1)||($type<3&&($root->Id()==$pchk->Id()||
				$this->checkAncestor($root,$pchk)===true))||
				($type<4&&$clvl>=0)) {
			$cleared = true;
		}
		else $cleared = false;
		return $cleared;
	}
	function filterFamily($ppid,$type,$valid8=true) {
		// $ppid is person id, $type is user type
		// 1-admin (alldata)
		// 2-superuser (all data on user's side)
		// 3-user (descendents of user's parents)
		// 4-viewer (immediate family and descendents)
		if ($this->ismain!==true)
			$this->throw_debug("Cannot filter main data!");
		// new (sub)-family
		$Fclass = get_class();
		$that = new $Fclass();
		$dbug = false;
		// people buffer to be processed
		$people = []; $levels = [];
		// filter level 4 - immediate family and descendents
		$test = $this->Person($ppid);
		if ($test===null)
			$this->throw_debug("Invalid PersonId! ($ppid)");
		$root = new Person();
		$root->copy($test); $clvl = 0;
		array_push($people,$root);
		array_push($levels,$clvl);
		while (count($people)>0) {
			$pchk = array_shift($people); // pchk is person
			$clvl = array_shift($levels);
			$test = $that->Person($pchk->Id());
			if ($test!==null) continue;
			$that->addPerson($pchk);
			if ($dbug) echo "@@ <PCHK:".$pchk->StrId()." (".$clvl.")\n";
			// add down-ward unions
			foreach ($pchk->link as $key1 => $val1) {
				if (substr($key1,0,6)=="unions") {
					$val1 = intval($val1);
					$test = $this->Union($val1);
					if ($test===null)
						$this->throw_debug("Invalid UnionId! ($val1)");
					$uchk = new Union();
					$uchk->copy($test);
					$uni1 = $that->Union($uchk->Id());
					if ($uni1!==null) continue;
					$that->addUnion($uchk);
					if ($dbug) echo "@@ +UCHK:".$uchk->StrId()."\n";
					// look for spouse!
					$next = null;
					if ($pchk->Id()===$uchk->IdF())
						$next = $uchk->IdM();
					else if ($pchk->Id()===$uchk->IdM())
						$next = $uchk->IdF();
					if ($next===null) // should not happen!
						$this->throw_debug('Cannot get spouse!');
					// in case already in...
					$test = $that->Person($next);
					if ($test===null) {
						$test = $this->Person($next);
						if ($test===null)
							$this->throw_debug("Invalid PersonId! ($next)");
						$curr = new Person();
						$curr->copy($test);
						// filter level 1 will get ancestors of spouse
						if ($type==1||$root->Id()!=$pchk->Id()) {
							array_push($people,$curr);
							array_push($levels,$clvl);
							if ($dbug) echo "@@ >PSPO:".$curr->StrId()."\n";
						}
						else {
							$that->addPerson($curr);
							if ($dbug) echo "@@ +PSPO:".$curr->StrId()."\n";
						}
					}
					$clvl++; // descend - child's level
					foreach ($uchk->link as $key2 => $val2) {
						if (substr($key2,0,5)==="child") {
							$val2 = intval($val2);
							$test = $that->Person($val2);
							if ($test===null) {
								$test = $this->Person($val2);
								if ($test===null)
									$this->throw_debug("Invalid PersonId! (".
										$val2.")");
								$curr = new Person();
								$curr->copy($test);
								// will always add children of current person!
								array_push($people,$curr);
								array_push($levels,$clvl);
								if ($dbug) echo "@@ >PCHI:".$curr->StrId()."\n";
							}
						}
					}
					$clvl--; // ascend - back to current level
				}
			}
			// filter ancestor data
			if (!$this->filterAncestor($pchk,$type,$root,$clvl))
				continue;
			$clvl--; // ascend to parent's level
			if ($dbug) echo "@@ ^^ ".$pchk->StrId()." (".$clvl.")\n";
			// add up-ward unions
			foreach ($pchk->link as $key1 => $val1) {
				if (substr($key1,0,7)==="parents") {
					$val1 = intval($val1);
					$test = $this->Union($val1);
					if ($test===null)
						$this->throw_debug("Invalid UnionId! ($val1)");
					$uchk = new Union();
					$uchk->copy($test);
					$uni1 = $that->Union($uchk->Id());
					if ($uni1!==null) continue;
					$that->addUnion($uchk);
					if ($dbug) echo "@@ +UNIP:".$uchk->StrId()."\n";
					// father
					$val2 = $uchk->idF();
					$test = $that->Person($val2);
					if ($test===null) {
						$test = $this->Person($val2);
						if ($test===null)
							$this->throw_debug("Invalid PersonId! (".$val2.")");
						$par1 = new Person();
						$par1->copy($test);
						// check permission level
						if ($this->filterAncestor($pchk,$type,$root,$clvl)) {
							array_push($people,$par1);
							array_push($levels,$clvl);
							if ($dbug) echo "@@ >PCHF:".$par1->StrId()."\n";
						}
						else {
							$that->addPerson($par1);
							if ($dbug) echo "@@ +PCHF:".$par1->StrId()."\n";
						}
					}
					// mother
					$val2 = $uchk->idM();
					$test = $that->Person($val2);
					if ($test===null) {
						$test = $this->Person($val2);
						if ($test===null)
							$this->throw_debug("Invalid PersonId! (".$val2.")");
						$par1 = new Person();
						$par1->copy($test);
						// check permission level
						if ($this->filterAncestor($pchk,$type,$root,$clvl)) {
							array_push($people,$par1);
							array_push($levels,$clvl);
							if ($dbug) echo "@@ >PCHM:".$par1->StrId()."\n";
						}
						else {
							$that->addPerson($par1);
							if ($dbug) echo "@@ +PCHM:".$par1->StrId()."\n";
						}
					}
					$clvl++; // descend to cousin's level
					foreach ($uchk->link as $key2 => $val2) {
						if (substr($key2,0,5)==="child") {
							$val2 = intval($val2);
							$test = $that->Person($val2);
							if ($test===null) {
								$test = $this->Person($val2);
								if ($test===null)
									$this->throw_debug("Invalid PersonId! (".
										$val2.")");
								$curr = new Person();
								$curr->copy($test);
								// check permission level
								if ($this->filterAncestor($pchk,$type,
										$root,$clvl)) {
									array_push($people,$curr);
									array_push($levels,$clvl);
									if ($dbug)
										echo "@@ >PCHC:".$curr->StrId()."\n";
								}
								else {
									$that->addPerson($curr);
									if ($dbug)
										echo "@@ +PCHC:".$curr->StrId()."\n";
								}
							}
						}
					}
					$clvl--; // ascend - parent's level
				}
			}
			$clvl++; // descend to current level
		}
		// remove missing links
		foreach ($that->people as &$pchk) {
			foreach ($pchk->link as $key1 => $val1) {
				if (substr($key1,0,7)=="parents") {
					$val1 = intval($val1);
					$uchk = $that->Union($val1);
					if ($uchk===null) {
						unset($pchk->link[$key1]);
						$pchk->link['pcount']--;
					}
				}
				else if (substr($key1,0,6)=="unions") {
					$val1 = intval($val1);
					$uchk = $that->Union($val1);
					if ($uchk===null) {
						unset($pchk->link[$key1]);
						$pchk->link['ucount']--;
					}
				}
			}
		}
		// prep sub-data links if needed
		if ($that->countP()<$this->countP()||$that->countU()<$this->countU()) {
			// set global id as reassign to 'local' id
			$step = 1;
			foreach ($that->people as &$pchk) {
				$pchk->Id2gId($step);
				$step++;
			}
			$step = 1;
			foreach ($that->unions as &$uchk) {
				$uchk->Id2gId($step);
				$step++;
			}
			// update links
			foreach ($that->people as &$pchk) {
				foreach ($pchk->link as $key1 => $val1) {
					if (substr($key1,0,7)=="parents") {
						$temp = $that->gUnion($val1);
						$pchk->link[$key1] = $temp->Id();
					}
					else if (substr($key1,0,6)=="unions") {
						$temp = $that->gUnion($val1);
						$pchk->link[$key1] = $temp->Id();
					}
				}
			}
			foreach ($that->unions as &$uchk) {
				$temp = $that->gPerson($uchk->IdF());
				$uchk->base['fatherid'] = $temp->Id();
				$temp = $that->gPerson($uchk->IdM());
				$uchk->base['motherid'] = $temp->Id();
				foreach ($uchk->link as $key1 => $val1) {
					if (substr($key1,0,5)==="child") {
						$temp = $that->gPerson($val1);
						$uchk->link[$key1] = $temp->Id();
					}
				}
			}
			// no longer main data!
			$that->ismain = false;
		}
		// copy logins
		if ($type<3&&$this->logins!==null) {
			$that->logins = $this->logins;
			foreach ($that->logins as &$pick) {
				if ($pick->base['id']==$root->gId())
					$pick->base['type'] = 1;
				$find = $that->gPerson($pick->Id());
				if ($find===null)
					$pick->base['id'] = 1; // default
				else
					$pick->base['id'] = $find->Id();
			}
		}
		if ($valid8) $that->valid8Data();
		return $that;
	}
	function domainData($main) {
		if (!is_subclass_of($main,'FamilyData'))
			$this->throw_debug("NOT a FamilyData!");
		if ($main->ismain!==true)
			$this->throw_debug("NOT Family master data!");
		if ($this->ismain===true)
			$this->throw_debug("NOT Family sub-data!");
		// 'recover' global id
		foreach ($this->people as &$pchk) {
			$temp = $main->Person($pchk->gId(),true);
			if ($temp===null) {
				// new person! get an id!
				$pick = $main->countP()+1;
				while ($main->Person($pick)!==null) $pick++;
			}
			else $pick = $temp->Id();
			$pchk->Id2gId($step); // 'save' local id in global
			$pchk->base['id'] = $pick;
		}
		foreach ($this->unions as &$uchk) {
			$temp = $main->Union($uchk->gId(),true);
			if ($temp===null) {
				// new union! get an id!
				$pick = $main->countU()+1;
				while ($main->Union($pick)!==null) $pick++;
			}
			else $pick = $temp->Id();
			$uchk->Id2gId($step);
			$uchk->base['id'] = $pick;
		}
		// update links
		foreach ($this->people as &$pchk) {
			foreach ($pchk->link as $key1 => $val1) {
				if (substr($key1,0,7)=="parents") {
					$temp = $this->gUnion($val1);
					$pchk->link[$key1] = $temp->Id();
				}
				else if (substr($key1,0,6)=="unions") {
					$temp = $this->gUnion($val1);
					$pchk->link[$key1] = $temp->Id();
				}
			}
		}
		foreach ($this->unions as &$uchk) {
			$temp = $this->gPerson($uchk->IdF());
			$uchk->base['fatherid'] = $temp->Id();
			$temp = $this->gPerson($uchk->IdM());
			$uchk->base['motherid'] = $temp->Id();
			foreach ($uchk->link as $key1 => $val1) {
				if (substr($key1,0,5)==="child") {
					$temp = $this->gPerson($val1);
					$uchk->link[$key1] = $temp->Id();
				}
			}
		}
		// remove global ids
		foreach ($this->people as &$pchk)
			unset($pchk->base['gid']);
		foreach ($this->unions as &$uchk)
			unset($uchk->base['gid']);
		$this->valid8Data();
		// now, part of 'main' data - ready for update/merge
		$this->ismain = true;
	}
}
//------------------------------------------------------------------------------
?>

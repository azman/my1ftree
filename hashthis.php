<?php
try {
	if (PHP_SAPI !== 'cli') { // or php_sapi_name()
		// returns html only if NOT on console?
		header('Content-Type: text/html; charset=utf-8');
		echo "<h1><p>Invalid access!</p></h1>".PHP_EOL;
		exit();
	}
	// check parameter
	if ($argc!=2) {
		throw new Exception("Must have exactly 1 parameter!");
	}
	$pass = $argv[1];
	$hash = hash('sha512',$pass,false);
	echo $pass." => ".$hash.PHP_EOL;
} catch( Exception $error ) {
	echo "Execution error! [".$error->getMessage()."]".PHP_EOL;
}
exit();
?>
